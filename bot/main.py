import logging
import uvicorn

from config import CCAS_WB_HOST, CCAS_WB_PORT

logger = logging.getLogger('startup')

if __name__ == '__main__':
    uvicorn.run('ccas:app', host=CCAS_WB_HOST, port=CCAS_WB_PORT)
