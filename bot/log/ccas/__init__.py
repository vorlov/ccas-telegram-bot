import logging

from . import formatters
from config import BASE_DIR


def setup_logger(name: str, level: int, log_format: str):
    log = logging.getLogger(name)
    log.setLevel(level)
    formatter = logging.Formatter(fmt=log_format)

    file_handler = logging.FileHandler(filename=BASE_DIR / 'ccas.log')
    stream_handler = logging.StreamHandler()

    file_handler.setLevel(logging.DEBUG)
    stream_handler.setLevel(logging.INFO)

    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    log.addHandler(stream_handler)
    log.addHandler(file_handler)


setup_logger(
    name='ccas',
    level=logging.INFO,
    log_format='[CCAS] %(asctime)s - %(levelname)s - %(message)s'
)
