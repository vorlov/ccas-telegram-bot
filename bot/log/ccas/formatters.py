import logging

ccas_formatter = logging.Formatter(fmt='[CCAS] %(asctime)s - %(levelname)s:%(name)s - %(message)s')
