import logging

startup_formatter = logging.Formatter(fmt='[STARTUP] %(asctime)s - %(levelname)s:%(name)s - %(message)s')
aiogram_formatter = logging.Formatter(fmt='[AIOGRAM] %(asctime)s - %(levelname)s:%(name)s - %(message)s')
ccas_api_formatter = logging.Formatter(fmt='[CCAS_API] %(asctime)s - %(levelname)s:%(name)s - %(message)s')
