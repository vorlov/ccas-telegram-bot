import logging

from . import formatters
from config import BASE_DIR


def setup_logger(name: str, level: int, log_format: str):
    log = logging.getLogger(name)
    log.setLevel(level)
    formatter = logging.Formatter(fmt=log_format)

    file_handler = logging.FileHandler(filename=BASE_DIR / 'bot.log')
    stream_handler = logging.StreamHandler()

    file_handler.setLevel(logging.DEBUG)
    stream_handler.setLevel(logging.INFO)

    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    log.addHandler(stream_handler)
    log.addHandler(file_handler)


setup_logger(
    name='startup',
    level=logging.INFO,
    log_format='[STARTUP] %(asctime)s - %(levelname)s - %(message)s'
)

setup_logger(
    name='aiogram',
    level=logging.INFO,
    log_format='[AIOGRAM] %(asctime)s - %(levelname)s - %(message)s'
)

setup_logger(
    name='ccas_api',
    level=logging.DEBUG,
    log_format='[CCAS_API] %(asctime)s - %(levelname)s - %(message)s'
)
