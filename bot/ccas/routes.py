import logging

from fastapi import APIRouter, Body, Response
from config import CCAS_WB_PATH

from bot import dp, bot as tg_bot, messages as tg_messages, keyboards as tg_keyboards
from bot.states import UserStates

router = APIRouter()
log = logging.getLogger('ccas')


@router.post(path=CCAS_WB_PATH)
async def process_request_from_ccas(payload: dict = Body(...)):
    telegram_id = payload['webhook_meta_data']['telegram_id']

    state = dp.current_state(user=telegram_id)

    log.info(f'Got event from CCAS for {telegram_id}')
    log.debug(f'Event for {telegram_id}, {payload}')

    if event := payload.get('MessageEvent'):
        await tg_bot.send_message(
            chat_id=telegram_id,
            text=event.get('body'),
            parse_mode='HTML'
        )

    elif event := payload.get('StatusEvent'):
        if event.get('status') == 'chat_timeout_waiting_for_agent':
            await tg_bot.send_message(
                chat_id=telegram_id,
                reply_markup=tg_keyboards.menu_kb(),
                text=tg_messages.AGENT_TIMEOUT,
                parse_mode='HTML'
            )

            await state.set_state(UserStates.menu.state)
            await state.reset_data()

        elif event.get('status') == 'chat_started':
            async with state.proxy() as data:
                tg_bot.delete_message(data['clock_message_id'])

            await tg_bot.send_message(
                chat_id=telegram_id,
                text=tg_messages.AGENT_CONNECT,
                parse_mode='HTML'
            )

            await state.set_state(UserStates.agent_talk.state)

        elif event.get('status') in [
            'chat_finished',
            'chat_error_non_recover',
            'chat_request_rejected_by_agent',
        ]:
            await tg_bot.send_message(
                chat_id=telegram_id,
                reply_markup=tg_keyboards.rating_keyboard(),
                text=tg_messages.AGENT_DISCONNECT,
                parse_mode='HTML'
            )

            await state.set_state(UserStates.rating.state)

    elif event := payload.get('TypingEvent'):
        if event.get('status') == 'composing':
            await tg_bot.send_chat_action(
                chat_id=telegram_id,
                action='typing'
            )

    return Response(status_code=200)
