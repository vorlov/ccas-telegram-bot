import asyncio

from bot import dp, register_views

if __name__ == '__main__':
    register_views(dp)
    asyncio.run(dp.start_polling())
