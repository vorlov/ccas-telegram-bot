from aiogram.dispatcher.filters.state import State, StatesGroup


class UserStates(StatesGroup):
    menu = State()
    agent_waiting = State()
    agent_talk = State()
    rating = State()
