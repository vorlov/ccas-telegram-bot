from aiogram import types


def menu_kb():
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(
        'Почати розмову з оператором 💬'
    )
    return markup


def end_kb():
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(
        'Закінчити розмову 🔚'
    )
    return markup


def rating_keyboard():
    markup = types.ReplyKeyboardMarkup(row_width=5, resize_keyboard=True)
    markup.add(
        '1', '2', '3', '4', '5',
        'Без оцінки ◽️'
    )
    return markup
