from aiogram import Dispatcher, Bot
from aiogram.contrib.fsm_storage.redis import RedisStorage2 as RedisStorage
from aiogram.dispatcher.filters import Text

from config import BOT_TOKEN, REDIS_DB, REDIS_HOST, REDIS_PORT

bot = Bot(token=BOT_TOKEN)
storage = RedisStorage(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
dp = Dispatcher(bot, storage=storage)


def register_views(dp: Dispatcher):
    from . import views
    from .states import UserStates

    dp.register_message_handler(views.process_start, commands=['start'], state='*')
    dp.register_message_handler(views.process_support_start, Text(equals='Почати розмову з оператором 💬'),
                                state=UserStates.menu)
    dp.register_message_handler(views.process_end_while_talking, Text(equals='Закінчити розмову 🔚'),
                                state=UserStates.agent_talk)
    dp.register_message_handler(views.process_end_while_waiting, Text(equals='Закінчити розмову 🔚'),
                                state=UserStates.agent_waiting)

    dp.register_message_handler(views.process_text_from_user, content_types=['text'],
                                state=(
                                    UserStates.agent_waiting,
                                    UserStates.agent_talk
                                ))

    dp.register_message_handler(views.process_rating, content_types=['text'],
                                state=UserStates.rating)
