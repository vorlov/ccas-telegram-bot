import config
from aiogram import types
from aiogram.dispatcher import FSMContext

from . import keyboards, messages
from .states import UserStates

from .api import ccas


async def process_start(message: types.Message):
    await message.reply(
        text=messages.START,
        reply_markup=keyboards.menu_kb(),
        parse_mode='HTML'
    )

    await UserStates.menu.set()


async def process_support_start(message: types.Message, state: FSMContext):
    ccas_session = await ccas.send_chat_request(
        author=message.from_user.username or (
                message.from_user.first_name + ' ' or '' + message.from_user.last_name or ''),
        feed_id=100000,
        csq_id=config.CCAS_CSQ,
        title='Meest Odoo Integration Test',
        telegram_id=message.from_user.id
    )

    mes = await message.reply(
        text=messages.AGENT_SEARCH + ' ' + messages.CLOCKS[0],
        reply_markup=keyboards.end_kb(),
        parse_mode='HTML'
    )

    await UserStates.agent_waiting.set()

    async with state.proxy() as data:
        data['clock_message_id'] = mes.message_id
        data['ccas_id'] = ccas_session['id']


async def process_text_from_user(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        ccas_id = data['ccas_id']

    await ccas.send_message(
        uuid=ccas_id,
        text=message.text
    )


async def process_end_while_waiting(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        ccas_id = data['ccas_id']

    await ccas.end_chat(ccas_id)

    await message.reply(
        text=messages.START,
        reply_markup=keyboards.menu_kb(),
        parse_mode='HTML'
    )

    await UserStates.menu.set()
    await state.reset_data()


async def process_end_while_talking(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        ccas_id = data['ccas_id']

    await ccas.end_chat(ccas_id)

    await message.reply(
        text=messages.CLIENT_DISCONNECT,
        reply_markup=keyboards.rating_keyboard(),
        parse_mode='HTML'
    )

    await UserStates.rating.set()


async def process_rating(message: types.Message, state: FSMContext):
    if message.text == 'Без оценки ◽️':
        await message.reply(
            text=messages.START,
            reply_markup=keyboards.menu_kb(),
            parse_mode='HTML'
        )

        await UserStates.menu.set()
        await state.reset_data()
    elif message.text in ['1', '2', '3', '4', '5']:
        async with state.proxy() as data:
            ccas_id = data['ccas_id']

        await ccas.send_feedback(
            uuid=ccas_id,
            rating=int(message.text)
        )

        await message.reply(
            text=messages.RATING_THANKS + '\n\n' + messages.START,
            reply_markup=keyboards.menu_kb(),
            parse_mode='HTML'
        )

        await UserStates.menu.set()
        await state.reset_data()
