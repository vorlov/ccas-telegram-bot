import logging
import aiohttp
from config import CCAS_WB_URL, CCAS_URL, CCAS_USER, CCAS_PASSWORD

auth = aiohttp.BasicAuth(login=CCAS_USER, password=CCAS_PASSWORD)
log = logging.getLogger()


async def send_chat_request(
        author: str,
        csq_id: int,
        feed_id: int,
        title: str,
        telegram_id: int
):
    body = {
        'sm_data': {
            'csq_id': csq_id,
            'feed_id': feed_id,
            'author': author,
            'title': title
        },
        'webhook': {
            'url': CCAS_WB_URL,
            'data': {'telegram_id': telegram_id}
        }
    }
    async with aiohttp.request('POST', CCAS_URL + '/v1/sm/init_chat',
                               json=body,
                               auth=auth) as r:
        if r.status == 200:
            log.debug(f'[API] Chat request to CCAS has been sent for {telegram_id}')
            return await r.json()
        else:
            log.warning(f'[API] Request to CCAS has been failed for {telegram_id}. Reason: {await r.text()}')
            r.raise_for_status()


async def send_message(uuid: str, text: str):
    body = {
        'session_id': uuid,
        'message': text
    }
    async with aiohttp.request('POST', CCAS_URL + '/v1/sm/send_message', json=body,
                               auth=auth) as r:
        if r.status == 200:
            log.debug(f'[API] Message to CCAS session ({uuid}) has been sent. Text: {text}')
        else:
            log.warning(f'[API] Request to CCAS has been failed. Reason: {await r.text()}')
            r.raise_for_status()


async def end_chat(uuid: str):
    body = {
        'session_id': uuid
    }

    async with aiohttp.request('POST', CCAS_URL + '/v1/sm/end_chat', json=body,
                               auth=auth) as r:
        if r.status == 200:
            log.debug(f'[API] End chat request to CCAS session ({uuid}) has been sent')
        else:
            log.warning(f'[API] Request to CCAS has been failed. Reason: {await r.text()}')
            r.raise_for_status()


async def send_feedback(uuid: str, rating: int):
    body = {
        'session_id': uuid,
        'rating': rating
    }

    async with aiohttp.request('POST', CCAS_URL + '/v1/sm/feedback', json=body,
                               auth=auth) as r:
        if r.status == 200:
            log.debug(f'[API] Feedback request to CCAS session ({uuid}) has been sent. Rating: {rating}')
        else:
            log.warning(f'[API] Request to CCAS has been failed. Reason: {await r.text()}')
            r.raise_for_status()
