from pathlib import Path
from configparser import ConfigParser

PROJECT_DIR = Path(__file__).resolve(strict=True).parent
BASE_DIR = PROJECT_DIR.parent

config = ConfigParser()
config.read(BASE_DIR / 'config.ini')

LOG_DIR = BASE_DIR / 'logs'
LOG_FILE = BASE_DIR / 'logging.yml'
LOG_DIR.mkdir(exist_ok=True)

BOT_TOKEN = config.get('BotConf', 'Token')

CCAS_WB = 'CCASReceive' in config.sections()
CCAS_WB_PATH = config.get('CCASReceive', 'WebhookPath')
CCAS_WB_HOST = config.get('CCASReceive', 'WebhookHost', fallback='localhost')
CCAS_WB_PORT = config.getint('CCASReceive', 'WebhookPort', fallback=8390)

CCAS = 'CCAS' in config.sections()
CCAS_URL = config.get('CCAS', 'ApiUrl', fallback='http://localhost:8290')
CCAS_WB_URL = config.get('CCAS', 'WebhookUrl')
CCAS_USER = config.get('CCAS', 'User')
CCAS_PASSWORD = config.get('CCAS', 'Password')
CCAS_CSQ = config.get('CCAS', 'Csq')

REDIS = 'Redis' in config.sections()
REDIS_HOST = config.get('Redis', 'Host', fallback='localhost')
REDIS_PORT = config.getint('Redis', 'Port', fallback=6379)
REDIS_DB = config.getint('Redis', 'DB', fallback=0)
