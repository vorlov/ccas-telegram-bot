FROM python:3.8

WORKDIR /app

COPY poetry.lock pyproject.toml /app/
RUN pip install "poetry==1.8.4"
RUN poetry config virtualenvs.create false \
    && poetry install --no-interaction --no-ansi

COPY . /app